package com.transfer;
import org.junit.Test;

import com.transfer.service.MoneyTransferService;
import javax.ws.rs.core.Application;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;

import static io.restassured.RestAssured.*;

public class IntegrationTransferTest extends JerseyTest {
	
	@Override
    protected Application configure() {
        return new ResourceConfig(MoneyTransferService.class);
    }
	
	@Test
    public void validTest() {
        given().
                pathParam("accountFrom", "123456").
                pathParam("accountTo", "789011").
                pathParam("amount", 50).
        when(). 
                post("/rest/transferService/{accountFrom}/{accountTo}/{amount}").
                then().
                statusCode(200);
    }
	
	@Test
    public void invalidAmountTest() {
        given().
                pathParam("accountFrom", "123456").
                pathParam("accountTo", "789011").
                pathParam("amount", 500).
        when(). 
                post("/rest/transferService/{accountFrom}/{accountTo}/{amount}").
                then().
                statusCode(400);
    }
	
	@Test
    public void invalidAccountToTest() {
        given().
                pathParam("accountFrom", "123456").
                pathParam("accountTo", "789888").
                pathParam("amount", 50).
        when(). 
                post("/rest/transferService/{accountFrom}/{accountTo}/{amount}").
                then().
                statusCode(400);
    }
	
	@Test
    public void invalidAccountFromTest() {
        given().
                pathParam("accountFrom", "123888").
                pathParam("accountTo", "789011").
                pathParam("amount", 50).
        when(). 
                post("/rest/transferService/{accountFrom}/{accountTo}/{amount}").
                then().
                statusCode(400);
    }
}
