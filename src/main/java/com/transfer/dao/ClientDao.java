package com.transfer.dao;

import java.util.HashMap;
import java.util.Map;

public enum ClientDao {

	instance;

    private Map<Integer, Client> contentProvider = new HashMap<>();

    private ClientDao() {

    	Client client1 = new Client(1, "Client 1");
        contentProvider.put(1, client1);
        
        Client client2 = new Client(2, "Client 2");
        contentProvider.put(2, client2);

    }
    
    public Map<Integer, Client> getModel(){
        return contentProvider;
    }
	
}
