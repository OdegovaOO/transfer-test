package com.transfer;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.transfer.service.MoneyTransferService;
import javax.ws.rs.core.Response;


public class TransferTest {
	
	@Test
    public void testTransferInvalidData() {
		MoneyTransferService transferService = new MoneyTransferService();
		Response response = transferService.transfer("123456", "123456", "123456");
		assertEquals(response.getStatus(), 400);
    }
	
	@Test
    public void testTransferValidData() {
		MoneyTransferService transferService = new MoneyTransferService();
		Response response = transferService.transfer("123456", "789011", "50");
		assertEquals(response.getStatus(), 200);
    }
}
