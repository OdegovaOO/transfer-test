package com.transfer.util;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.stream.Collectors;

import com.transfer.dao.Account;
import com.transfer.dao.AccountDao;
import com.transfer.dao.Transaction;
import com.transfer.dao.TransactionDao;

public class TransferUtils {
	
	public static Integer getTransferId() {
		Map<Integer, Transaction> transactions = TransactionDao.instance.getModel();
		OptionalInt lastId = transactions.values().stream().mapToInt(item -> item.getId()).max();
		return lastId.isPresent() ? lastId.getAsInt() + 1 : 1;
	}
	
	public static Account getAccount(String name) throws Exception {
		Map<Integer, Account> accounts = AccountDao.instance.getModel();
		List<Account> choosedAccounts = accounts.values().stream().filter(item -> name.equals(item.getName())).collect(Collectors.toList());
		if (choosedAccounts.isEmpty() || choosedAccounts.size() > 1) {
			throw new Exception("can't find account");
		}
		return choosedAccounts.get(0);
	}
	
	public static void createTransaction(Account accountFrom, Account accountTo, BigDecimal amount, Currency currency) {
		Integer id = TransferUtils.getTransferId();
        Transaction transaction = new Transaction(id, accountFrom, accountTo, amount, currency);
        TransactionDao.instance.getModel().put(id, transaction);
	}
	
	public static void transfer(Account accountFrom, Account accountTo, BigDecimal amount, Currency currency) throws Exception {
		//check that sender has enough money
		if (accountFrom.getAmount() != null && accountFrom.getAmount().compareTo(amount) >= 0) {
			accountFrom.setAmount(accountFrom.getAmount().subtract(amount));
			accountTo.setAmount(accountTo.getAmount().add(amount));
			createTransaction(accountFrom, accountTo, amount, currency); 
		} else {
			throw new Exception("There is not enough money to make the transfer");
		}
	}
}
