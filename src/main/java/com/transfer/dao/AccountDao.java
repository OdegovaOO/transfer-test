package com.transfer.dao;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

public enum AccountDao {

	instance;

    private Map<Integer, Account> contentProvider = new HashMap<>();

    private AccountDao() {
    	//assume that they have the only one currency
    	Currency EUR = Currency.getInstance("EUR");
    	
    	Client client1 = ClientDao.instance.getModel().get(0);
    	Client client2 = ClientDao.instance.getModel().get(1);

        Account account1 = new Account(1, "123456", client1, BigDecimal.valueOf(100.0), EUR);
        contentProvider.put(1, account1);
        
        Account account2 = new Account(2, "789011", client2, BigDecimal.valueOf(200.0), EUR);
        contentProvider.put(2, account2);

    }
    
    public Map<Integer, Account> getModel(){
        return contentProvider;
    }
	
}
