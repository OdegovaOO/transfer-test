package com.transfer.service;

import java.math.BigDecimal;
import java.util.Currency;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import com.transfer.dao.Account;
import com.transfer.util.TransferUtils;


@Path("/transferService")
public class MoneyTransferService {
	
    @POST
    @Path("/{accountFrom}/{accountTo}/{amount}")
    public Response transfer(@PathParam("accountFrom") String accountFromName,
    		@PathParam("accountTo") String accountToName,
    		@PathParam("amount") String amountString) {
    	
    	Account accountFrom;
    	Account accountTo;
    	BigDecimal amount;
    	
    	//assume, that we have only one currency
    	Currency currency = Currency.getInstance("EUR");
    	
    	try {
    		accountFrom = TransferUtils.getAccount(accountFromName);
    	} catch (Exception e) {
    		return Response.status(400).entity("Account from hasn't been found").build();
    	}
    	
    	try {
    		accountTo = TransferUtils.getAccount(accountToName);
    	} catch (Exception e) {
    		return Response.status(400).entity("Account to hasn't been found").build();
    	}
    	
    	try {
    		amount = new BigDecimal(amountString);
    	} catch (Exception e) {
    		return Response.status(400).entity("Amount has errors").build();
    	}
    	
    	try {
    		TransferUtils.transfer(accountFrom, accountTo, amount, currency);
    	} catch (Exception e) {
    		return Response.status(400).entity(e.getMessage()).build();
    	}

        return Response.status(200).entity("Transaction has been done").build();
    }
}
