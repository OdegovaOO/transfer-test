package com.transfer.dao;

import java.util.HashMap;
import java.util.Map;

public enum TransactionDao {

	instance;

    private Map<Integer, Transaction> contentProvider = new HashMap<>();

    private TransactionDao() {

    }
    
    public Map<Integer, Transaction> getModel(){
        return contentProvider;
    }
	
}
