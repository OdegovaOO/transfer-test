package com.transfer.dao;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;

public class Transaction {

	private Integer id;
	private Date date;
	private Account accountFrom;
	private Account accountTo;
	private BigDecimal amount;
	private Currency currency;
	
	public Transaction() {
		
	}
	
	public Transaction(Integer id, Account accountFrom, Account accountTo, BigDecimal amount, Currency currency) {
		this.id = id;
		this.date = new Date();
		this.accountFrom = accountFrom;
		this.accountTo = accountTo;
		this.amount = amount;
		this.currency = currency;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Account getAccountFrom() {
		return accountFrom;
	}

	public void setAccountFrom(Account accountFrom) {
		this.accountFrom = accountFrom;
	}

	public Account getAccountTo() {
		return accountTo;
	}

	public void setAccountTo(Account accountTo) {
		this.accountTo = accountTo;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
}
