package com.transfer.dao;

import java.math.BigDecimal;
import java.util.Currency;

public class Account {

	private Integer id;
	private String name;
	private Client client;
	private BigDecimal amount;
	
	//assume that account can have only single currency
	private Currency currency;
	
	
	public Account() {
		
	}
	
	public Account(Integer id, String name, Client client, BigDecimal amount, Currency currency) {
		this.id = id;
		this.name = name;
		this.client = client;
		this.amount = amount;
		this.currency = currency;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Client getClient() {
		return client;
	}
	
	public void setClient(Client client) {
		this.client = client;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
}
